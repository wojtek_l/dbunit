package org.lobacz.dbunit.demo.basic;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.lobacz.dbunit.demo.DbConfiguration;

public class GenericTest {

	public IDatabaseConnection getConnection() throws Exception {
		
//		Class driverClass = Class.forName(DbConfiguration.get_driverClass());
		Connection jdbcConnection = DriverManager.getConnection(DbConfiguration.getJdbcConnection());
 
		return new DatabaseConnection(jdbcConnection);
	}
	
	public void cleanInsert(String filename) throws Exception {
		IDatabaseConnection connection = getConnection();
		IDataSet dataSet =  readDataSet(filename);
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
	}
	
	public IDataSet readDataSet(String filename) throws Exception {
		return new FlatXmlDataSetBuilder().build(new File(filename));
	}

 
}
