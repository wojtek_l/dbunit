package org.lobacz.dbunit.demo.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Person")
@SequenceGenerator(name="PersonSeq", sequenceName="PERSON_SEQ")
public class Person {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PersonSeq")
    private int id;
	
    
    private String firstname;
    
    private String lastname;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
    
    @Override
    public String toString() {
    
    	return "Person [ id=" + id + ", firstname=" +  firstname + ", lastname= "  + lastname +" ]";
    }
    
}
