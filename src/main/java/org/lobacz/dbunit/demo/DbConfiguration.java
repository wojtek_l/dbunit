package org.lobacz.dbunit.demo;

public class DbConfiguration {

	private static String _testDir = System.getProperty("user.dir") + "\\datasets\\output";
	private static String inputDir = System.getProperty("user.dir") + "\\datasets\\input";
	private static String outputDir = System.getProperty("user.dir") + "\\datasets\\output";
	
	private static String dbFile = "dbDump.xml";
	private static String driverClass = "org.apache.derby.jdbc.ClientDriver";
	private static String jdbcConnection = "jdbc:derby://localhost:1527/testDb";
	
	public static String getTestDir() {
		return _testDir;
	}
	public static String getDbFile() {
		return dbFile;
	}
	public static String getDriverClass() {
		return driverClass;
	}
	public static String getJdbcConnection() {
		return jdbcConnection;
	}
 
	public static String getInputDir() {
		return inputDir;
	}
	public static String getOutputDir() {
		return outputDir;
	}
	
	
	
	
}
