package org.lobacz.dbunit.demo.tests;

import java.io.File;
import java.io.FileOutputStream;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.lobacz.dbunit.demo.DbConfiguration;
import org.lobacz.dbunit.demo.basic.GenericTest;

//
// 03.1 clean insert with DatabaseOperation
// 03.2 use DatabaseSequenceFilter to export only desired tables (in this example table: person)
// 03.3 create FilteredDataSet 
// 03.4 write to file (hint: FlatXmlDataSet)
// 03.5 create dataset from file
//
// 03.6 why do we get exception?
public class ImportExportTest extends GenericTest {

	@Test
	public void insertTest() throws Exception {
		 IDataSet dataSet = readDataSet("datasets//input//persons-init.xml");
		 // 03.1 put some code here
	}
 

	public void databaseExport(File file) throws Exception {
		IDatabaseConnection connection = getConnection();

		// 03.2 put some code
		// 03.3 put some code
		// 03.4 put some code
	}

	
 
	
	@Test
	public void exportImportTest() throws Exception {
		
		File file = new File("datasets//output/dbDump.xml");
		
		databaseExport(file);
		ITable actualTable = getConnection().createDataSet().getTable("person");

		IDataSet expectedDataSet = null; // 03.6 put some code here
		ITable expectedTable = expectedDataSet.getTable("person");
		
		Assertion.assertEquals(expectedTable, actualTable);

	}

 



 
}
