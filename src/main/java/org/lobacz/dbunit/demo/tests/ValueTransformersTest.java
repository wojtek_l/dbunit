package org.lobacz.dbunit.demo.tests;

import java.util.ArrayList;
import java.util.Collection;

import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.lobacz.dbunit.demo.basic.GenericTest;
import org.lobacz.dbunit.demo.dataset.ValueTransformerDataSet;
import org.lobacz.dbunit.demo.transformers.ValueTransformer;

//
// 04 Value transformers test
// 04.1 create new ValueTransformer (PlaceHolder) and use it for param: version 
// 04.2 create new ValueTransformer (String Reversal) and use it 
//
public class ValueTransformersTest extends GenericTest {

	@Test
	public void placeholdersTest() throws Exception {

		IDataSet dataSet = readDataSet("datasets//input//project-init.xml");
		
		Collection<ValueTransformer> transformers = new ArrayList<ValueTransformer>();
		// 04.1 put some code here
		
		ValueTransformerDataSet transformedDataSet = new ValueTransformerDataSet(dataSet, transformers );
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), transformedDataSet); 

	}

	
	@Test
	public void reverseTransfomermsTest() throws Exception {

		IDataSet dataSet = readDataSet("datasets//input//project-init.xml");
		
		Collection<ValueTransformer> transformers = new ArrayList<ValueTransformer>();
		// 04.1 put some code here 
		// 04.2 put some code here
		
		ValueTransformerDataSet transformedDataSet = new ValueTransformerDataSet(dataSet, transformers );
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), transformedDataSet); 

	}
	
}
