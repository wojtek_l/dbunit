package org.lobacz.dbunit.demo.tests;

import java.io.FileOutputStream;

import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;
import org.lobacz.dbunit.demo.basic.GenericTest;

public class QueryExportTask extends GenericTest {

    // Task 02: partial database export
	//
	// 02.1 export table backoffice
	// 02.2 export query 'select * from person'
	// 02.3 change table name to 'employee'
	//
	@Test
	public void queryExport_Test() throws Exception {

	    QueryDataSet partialDataSet = new QueryDataSet(getConnection());

	    // put some code here
	    
	    FlatXmlDataSet.write(partialDataSet,  new FileOutputStream("datasets/output/queryExport.xml"));
		
	}
    
}
