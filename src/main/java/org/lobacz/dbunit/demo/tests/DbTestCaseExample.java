package org.lobacz.dbunit.demo.tests;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.assertion.DiffCollectingFailureHandler;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Test;

// Task 01: basic operations
//
// 01.1 configure database connection using System.setProperty and PropertiesBasedJdbcDatabaseTester
// 01.2 configure log4j using BasicConfigurator
// 01.3 set some properties for example PROPERTY_BATCH_SIZE  (hint: DatabaseConfig)
// 01.4 print table names and row count (hint: getConnection()
// 01.5 get from db table person and compare with expectedDataSet.xml (use class: Assertion)
// 01.6 check different handlers: DiffCollectingFailureHandler and create custom failure handler
public class DbTestCaseExample extends DBTestCase {

	private static final Log logger = LogFactory.getLog(DbTestCaseExample.class);

	public DbTestCaseExample(String name) {
		super(name);
		logger.info("Test class constructor");
		// 01.1-2 put some code
	}

	protected void setUpDatabaseConfig(DatabaseConfig config) {
		// 01.3 put some code
	}

	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("datasets/input/persons-init.xml"));
	}

	@Test
	public void connectionTest() throws Exception {
		
		// 01.4 put some code

	}

	@Test
	public void compareTable() throws Exception {
		
		// 01.5 put some code

		// a) get table from db
		
		// b) get table from file
		
		// c) compare them

		// d) try different failure handlers

	}

}
