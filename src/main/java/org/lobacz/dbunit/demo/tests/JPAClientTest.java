package org.lobacz.dbunit.demo.tests;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.lobacz.dbunit.demo.jpa.model.Person;

public class JPAClientTest {
	
	private static final String PERSISTENCE_UNIT_NAME = "test";
	
	private EntityManagerFactory factory;

	private EntityManager em; 
	
	private static final Log logger = LogFactory.getLog(JPAClientTest.class);

	
	@Before
	public void init(){
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}
	
	@Test
	public void test() {
		
		Query q = em.createQuery("select p from Person p" , Person.class);
		List<Person> personList = q.getResultList();
		for (Person person : personList) {
			logger.info(person);
		}
		logger.info("Size: " + personList.size());

		em.getTransaction().begin();
		
		Person person = new Person();
		person.setFirstname("Wojtek");
		person.setLastname("DbUnit");
		em.persist(person);
		
		em.getTransaction().commit();

		em.close();
	}
}
