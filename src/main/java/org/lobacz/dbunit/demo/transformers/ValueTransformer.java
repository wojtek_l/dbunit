package org.lobacz.dbunit.demo.transformers;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;

public interface ValueTransformer
{

    public Object getTransformedValue(Object value, ITable table, int row, String columnName) throws DataSetException;

}
