package org.lobacz.dbunit.demo.dataset;

import java.util.Collection;

import org.dbunit.dataset.AbstractTable;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.lobacz.dbunit.demo.transformers.ValueTransformer;

// make class nonabstract
// implement required classes
public abstract class ValueTransformerTable extends AbstractTable
{

    private final ITable table;

    private final Collection<ValueTransformer> transformers;

 
    public ValueTransformerTable(ITable tableParam, Collection<ValueTransformer> transformersParam)
    {
        table = tableParam;
        transformers = transformersParam;
    }

 
    @Override
    public Object getValue(int row, String columnName) throws DataSetException
    {
    	// get value from table
    	// iterate through transfomers and change value 
    	// return a proper result
        return null;
    }


    @Override
    public String toString()
    {
        return table.getTableMetaData().getTableName();
    }

}
