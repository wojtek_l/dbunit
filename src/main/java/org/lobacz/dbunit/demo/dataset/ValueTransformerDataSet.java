package org.lobacz.dbunit.demo.dataset;

import java.util.Collection;

import org.dbunit.dataset.AbstractDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.DefaultTableIterator;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.lobacz.dbunit.demo.transformers.ValueTransformer;


public class ValueTransformerDataSet extends AbstractDataSet
{
    private final Collection<ITable> tables;


    public ValueTransformerDataSet(IDataSet dataSet, Collection<ValueTransformer> transformers) throws DataSetException
    {
    	// init tables properly
    	tables = null;
    	// add ValueTransformerTable 
    }


    @Override
    protected ITableIterator createIterator(boolean reversed) throws DataSetException
    {
        return new DefaultTableIterator(tables.toArray(new ITable[tables.size()]), reversed);
    }

}
