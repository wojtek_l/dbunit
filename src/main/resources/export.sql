DROP SEQUENCE PERSON_SEQ restrict;
DROP TABLE Person;
DROP TABLE BACKOFFICE;
DROP TABLE project;

--------------------------------------------------------
--  File created - Monday-July-20-2015   
--------------------------------------------------------
 
  CREATE TABLE BACKOFFICE 
   (	
	BACKOFFICEID Double, 
	SHORTDESC VARCHAR(30), 
	FULLDESC VARCHAR(30), 
	AVAILABLE CHAR, 
	SYSTEMAVAILABLE Double DEFAULT 1, 
	CONNECTIONAVAILABLE Double DEFAULT 1, 
	AUDITID Double
   );
   
   CREATE TABLE PROJECT 
   (	
	version VARCHAR(30),  
	description VARCHAR(300),  
	vendor VARCHAR(30),
	budget double
   );

create table Person
(
	id varchar(20),
	firstname varchar(50),
	lastname varchar(50),
	age integer
);

CREATE SEQUENCE PERSON_SEQ increment by 1;
